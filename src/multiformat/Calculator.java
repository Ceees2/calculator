/*
 * (C) Copyright 2005 Davide Brugali, Marco Torchiano
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307  USA
 */
package multiformat;

import java.util.Stack;

/**
 * The multiformat calculator
 */
public class Calculator {
  private Rational operand_0 = new Rational();
  private Rational operand_1 = new Rational();
  private Stack<Rational> operands = new Stack<Rational>();
  
  // The current format of the calculator
  private Format format = new FixedPointFormat();
  // The current numberbase of the calculator
  private Base base = new DecimalBase();

  public void addOperand(String newOperand) throws FormatException {
	  operands.push(format.parse(newOperand, base));
  }

  public void add(){
	  Rational answer = new Rational();
	  while(!operands.empty())
		  answer = answer.plus(operands.pop());
	  operands.push(answer);
  }
  public void subtract() {
	  Rational answer = new Rational();
	  while(!operands.empty())
		  answer = answer.minus(operands.pop());
	  operands.push(answer);
  }
  public void multiply() {
	  if(!operands.empty()) {
		  Rational answer = operands.pop();
		  while(!operands.empty())
			  answer = answer.mul(operands.pop());
		  operands.push(answer);
	  }
	  else
		  operands.push(new Rational());
  }
  public void divide() {
	  if(!operands.empty()){
		  Rational answer = operands.pop();
		  while(!operands.empty())
				answer = operands.pop().div(answer);
		  operands.push(answer);
	  }else
		  operands.push(new Rational());
  }
  public void delete() {
	  while(!operands.empty())
		  operands.pop();
  }

  public String getOperand() {
	  if(!operands.empty()) {
		  Rational answer = operands.pop();
		  operands.push(answer);
		  return format.toString(answer, base);
	  }
	  else
		  return format.toString(new Rational(), base);
  }
  
  public String getOperandTest() {
	  if(!operands.empty()) {
		  Rational answer = operands.pop();
		  return format.toString(answer, base);
	  }
	  else
		  return format.toString(new Rational(), base);
  }

  public void setBase(Base newBase){
    base = newBase;
  }
  public Base getBase(){
    return base;
  }
  public void setFormat(Format newFormat){
    format = newFormat;
  }
  public Format getFormat(){
    return format;
  }
}