package multiformat;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CalculatorController extends JPanel implements ActionListener {
	private CalculatorModel model;

	JButton nul;
	JButton een;
	JButton twee;
	JButton drie;
	JButton vier;
	JButton vijf;
	JButton zes;
	JButton zeven;
	JButton acht;
	JButton negen;

	JButton plus;
	JButton min;
	JButton keer;
	JButton delen;
	JButton dec;
	JButton bin;
	JButton oct;
	JButton hex;
	JButton rat;
	JButton fixed;
	JButton floatt;
	JButton del;
	
	JButton submit;

	public CalculatorController(CalculatorModel model) {
		this.model = model;

		init();
	}

	public void init() {
		this.setLayout(new GridLayout(0, 3));

		nul = new JButton("0");
		nul.addActionListener(this);
		een = new JButton("1");
		een.addActionListener(this);
		twee = new JButton("2");
		twee.addActionListener(this);
		drie = new JButton("3");
		drie.addActionListener(this);
		vier = new JButton("4");
		vier.addActionListener(this);
		vijf = new JButton("5");
		vijf.addActionListener(this);
		zes = new JButton("6");
		zes.addActionListener(this);
		zeven = new JButton("7");
		zeven.addActionListener(this);
		acht = new JButton("8");
		acht.addActionListener(this);
		negen = new JButton("9");
		negen.addActionListener(this);

		plus = new JButton("+");
		plus.addActionListener(this);
		min = new JButton("-");
		min.addActionListener(this);
		keer = new JButton("*");
		keer.addActionListener(this);
		delen = new JButton("/");
		delen.addActionListener(this);
		dec = new JButton("dec");
		dec.addActionListener(this);
		bin = new JButton("bin");
		bin.addActionListener(this);
		oct = new JButton("oct");
		oct.addActionListener(this);
		hex = new JButton("hex");
		hex.addActionListener(this);
		rat = new JButton("rat");
		rat.addActionListener(this);
		fixed = new JButton("fixed");
		fixed.addActionListener(this);
		floatt = new JButton("float");
		floatt.addActionListener(this);
		del = new JButton("del");
		del.addActionListener(this);
		submit = new JButton("submit");
		submit.addActionListener(this);

		add(een);
		add(twee);
		add(drie);
		add(vier);
		add(vijf);
		add(zes);
		add(zeven);
		add(acht);
		add(negen);

		add(plus);
		add(nul);
		add(min);
		add(keer);
		add(delen);
		add(dec);
		add(bin);
		add(oct);
		add(hex);
		add(rat);
		add(fixed);
		add(floatt);
		add(del);
		
		add(submit);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == een) {
			model.een();
		}
		if (e.getSource() == twee) {
			model.twee();
		}
		if (e.getSource() == drie) {
			model.drie();
		}
		if (e.getSource() == vier) {
			model.vier();
		}
		if (e.getSource() == vijf) {
			model.vijf();
		}
		if (e.getSource() == zes) {
			model.zes();
		}
		if (e.getSource() == zeven) {
			model.zeven();
		}
		if (e.getSource() == acht) {
			model.acht();
		}
		if (e.getSource() == negen) {
			model.negen();
		}
		if (e.getSource() == nul) {
			model.nul();
		}
		

		if (e.getSource() == plus) {
			model.plus();
		}
		if (e.getSource() == min) {
			model.min();
		}
		if (e.getSource() == keer) {
			model.keer();
		}
		if (e.getSource() == delen) {
			model.delen();
		}
		if (e.getSource() == dec) {
			model.dec();
		}
		if (e.getSource() == bin) {
			model.bin();
		}
		if (e.getSource() == oct) {
			model.oct();
		}
		if (e.getSource() == hex) {
			model.hex();
		}
		if (e.getSource() == rat) {
			model.rat();
		}
		if (e.getSource() == fixed) {
			model.fixed();
		}
		if (e.getSource() == floatt) {
			model.floatt();
		}
		if (e.getSource() == del) {
			model.del();
		}

		if (e.getSource() == submit) {
			model.submit();
		}
	}
}
