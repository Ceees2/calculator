package multiformat;


import java.awt.BorderLayout;

import javax.swing.JApplet;

@SuppressWarnings("serial")
public class CalculatorMVC extends JApplet {
	CalculatorModel model;             //het model
	CalculatorView calculatorView;  // view
	HistoryView historyView;  // view
	CalculatorController controller;  // controller
	
	public CalculatorMVC() {
		init();
	}
	
	public static void main(String[] args){
		new CalculatorMVC();
	}
	
	public void init()
	{
		resize(250,200); 
		// Maak het model
		model = new CalculatorModel();

        // Maak de views
        calculatorView = new CalculatorView();        
        getContentPane().add(calculatorView,BorderLayout.NORTH);
        
		controller = new CalculatorController(model);
        getContentPane().add(controller,BorderLayout.CENTER);

        historyView = new HistoryView();        
        getContentPane().add(historyView,BorderLayout.SOUTH);
        
        model.addActionListener(calculatorView);
        model.addActionListener( historyView);
        
        /*
        // Registreer de views bij het model
        model.addActionListener(tekstView);
        model.addActionListener(dobbelsteenView);
        model.addActionListener(throwsView);*/
        
        // Eerste worp
       //  model.gooi();
	}
}
