package multiformat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CalculatorModel {
	private Calculator calc;
	private ArrayList<ActionListener> actionListenerList;
	private String invoer;
	private boolean invoeren;
	private int aantal;

	public CalculatorModel() {
		actionListenerList = new ArrayList<ActionListener>();
		calc = new Calculator();
		invoer = "";
		invoeren = true;
		aantal = 0;
	}

	public void incAantal() {
		aantal++;
	}

	public int getAantal() {
		return aantal;
	}

	public void een() {
		invoer += "1";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void twee() {
		invoer += "2";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void drie() {
		invoer += "3";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void vier() {
		invoer += "4";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void vijf() {
		invoer += "5";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void zes() {
		invoer += "6";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void zeven() {
		invoer += "7";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void acht() {
		invoer += "8";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void negen() {
		invoer += "9";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void nul() {
		invoer += "0";
		invoeren = true;
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void plus() {
		submitInvoer();
		calc.add();
		invoeren = false;
		incAantal();
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void min() {
		submitInvoer();
		calc.subtract();
		invoeren = false;
		incAantal();
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void keer() {
		submitInvoer();
		calc.multiply();
		invoeren = false;
		incAantal();
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void delen() {
		submitInvoer();
		calc.divide();
		invoeren = false;
		incAantal();
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void dec() {
		submitInvoer();
		calc.setBase(new DecimalBase());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void bin() {
		submitInvoer();
		calc.setBase(new BinaryBase());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void oct() {
		submitInvoer();
		calc.setBase(new OctalBase());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void hex() {
		submitInvoer();
		calc.setBase(new HexBase());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void rat() {
		submitInvoer();
		calc.setFormat(new RationalFormat());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void fixed() {
		submitInvoer();
		calc.setFormat(new FixedPointFormat());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void floatt() {
		submitInvoer();
		calc.setFormat(new FloatingPointFormat());
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void del() {
		invoeren = false;
		calc.delete();
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void submit() {
		submitInvoer();
		processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}

	public void submitInvoer() {
		if (invoer != "") {
			try {
				calc.addOperand(invoer);
				invoer = "";
			} catch (FormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void addActionListener(ActionListener l) {
		actionListenerList.add(l);
	}

	public void removeActionListener(ActionListener l) {
		if (actionListenerList.contains(l))
			actionListenerList.remove(l);
	}

	private void processEvent(ActionEvent e) {
		for (ActionListener l : actionListenerList)
			l.actionPerformed(e);
	}

	public String getWaarde() {
		if (invoeren) {
			return invoer;
		} else {
			return calc.getOperand().toString();
		}
	}
}
