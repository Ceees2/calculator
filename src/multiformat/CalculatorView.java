package multiformat;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class CalculatorView extends JPanel implements ActionListener {
	private JTextField result;

	public CalculatorView() {
	    this.setLayout(new GridLayout());
		result = new JTextField();
		add(result);
	}

	public void actionPerformed(ActionEvent e) {
		System.out.println("event");
		CalculatorModel d = (CalculatorModel) e.getSource();
		result.setText(d.getWaarde());
		System.out.println(d.getWaarde());
	}

}
