package multiformat;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class HistoryView  extends JPanel implements ActionListener {
	private JTextField result;
	private final JLabel aantalLabel;

	public HistoryView() {
	    this.setLayout(new GridLayout(0,2));
	    
		aantalLabel = new JLabel("Aantal: ");
		result = new JTextField();
		
		add(aantalLabel);
		add(result);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CalculatorModel d = (CalculatorModel) e.getSource();
		result.setText(String.valueOf(d.getAantal()));
	}

}
