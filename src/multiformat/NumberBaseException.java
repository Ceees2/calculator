package multiformat;

@SuppressWarnings("serial")
public class NumberBaseException extends RuntimeException {
	public NumberBaseException(char number, String baseName) {
		super("Symbool " + number + " bestaat niet in stelsel " + baseName);
	}
}