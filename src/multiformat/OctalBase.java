package multiformat;

/**
 * Octal numbering base
 * @author C.Wiersma
 * @author E.Lenting
 * @version 1.0
 */
public class OctalBase extends Base {
	
	/**
	 * Constructor for OctalBase
	 */
	public OctalBase() {
		super("oct",8,"01234567");
		}
	}
