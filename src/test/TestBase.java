package test;
import junit.framework.TestCase;
import multiformat.*;
import org.junit.Before;
import org.junit.Test;

public class TestBase extends TestCase {
	BinaryBase binaryBase;
	OctalBase octalBase;
	HexBase hexBase;
	
	public TestBase(String arg0) {
		super(arg0);
	}
	
	@Before
	public void setUp() {
		binaryBase = new BinaryBase();
		octalBase = new OctalBase();
		hexBase = new HexBase();
	}
	
	@Test
	public void testBinaryBase() {
		binaryBase.parse("1");
		binaryBase.parse("2");
	}
	
	@Test
	public void testOctalBase(){
		octalBase.parse("7");
		octalBase.parse("8");
	}
	
	@Test
	public void testHexBase() {
		hexBase.parse("1A");
		hexBase.parse("1a");
	}
}
